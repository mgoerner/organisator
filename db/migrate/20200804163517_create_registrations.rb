class CreateRegistrations < ActiveRecord::Migration[5.2]
  def change
    create_table :registrations do |t|
      t.string :token
      t.string :status
      t.references :event, foreign_key: true
      t.references :contact, foreign_key: true
      t.string :plz
      t.string :city
      t.string :emergency_nr
      t.text :participants
      t.boolean :accepts_storage

      t.timestamps
    end
  end
end
