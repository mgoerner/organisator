class CreateMailings < ActiveRecord::Migration[5.2]
  def change
    create_table :mailings do |t|
      t.string :status
      t.references :contact, foreign_key: true
      t.references :packet, polymorphic: true
      t.datetime :send_at
      t.integer :clicks
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
