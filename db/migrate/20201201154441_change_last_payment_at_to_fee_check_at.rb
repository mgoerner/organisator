class ChangeLastPaymentAtToFeeCheckAt < ActiveRecord::Migration[5.2]
  def change
    rename_column :memberships, :last_payment_at, :fee_check_at
  end
end
