class AddPhonenrToMembership < ActiveRecord::Migration[5.2]
  def change
    add_column :memberships, :phone_nr, :string
  end
end
