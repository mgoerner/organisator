class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :status
      t.references :parent, foreign_key: { to_table: :pages }
      t.string :path
      t.string :title
      t.string :menu
      t.integer :position, default: 1, null: false
      t.string :description
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
