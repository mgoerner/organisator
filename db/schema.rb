# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_11_145008) do

  create_table "accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "login"
    t.string "password_digest"
    t.bigint "contact_id"
    t.string "displayname"
    t.string "label"
    t.text "description"
    t.string "reset_token"
    t.datetime "reset_expires_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_accounts_on_contact_id"
  end

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "arguments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.bigint "proposal_id"
    t.string "title"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_arguments_on_account_id"
    t.index ["proposal_id"], name: "index_arguments_on_proposal_id"
  end

  create_table "articles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.bigint "author_id"
    t.datetime "published_at"
    t.string "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_id"], name: "index_articles_on_author_id"
  end

  create_table "comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.string "entity_type"
    t.bigint "entity_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_comments_on_account_id"
    t.index ["entity_type", "entity_id"], name: "index_comments_on_entity_type_and_entity_id"
  end

  create_table "contacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.string "plz"
    t.boolean "read_privacy"
    t.boolean "wants_news"
    t.boolean "offers_help"
    t.datetime "news_last_sent"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "street"
    t.string "housenr"
    t.string "city"
    t.boolean "wants_form"
  end

  create_table "contentparts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "kind"
    t.integer "position"
    t.string "container_type"
    t.bigint "container_id"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["container_type", "container_id"], name: "index_contentparts_on_container_type_and_container_id"
  end

  create_table "decisions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.bigint "account_id"
    t.text "description"
    t.datetime "end_proposals_at"
    t.datetime "end_voting_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_decisions_on_account_id"
  end

  create_table "events", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "description"
    t.string "location"
    t.text "details"
    t.string "url"
    t.datetime "registration_starts_at"
    t.datetime "registration_ends_at"
    t.integer "max_registrations"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mailings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "contact_id"
    t.string "packet_type"
    t.bigint "packet_id"
    t.datetime "send_at"
    t.integer "clicks"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_mailings_on_contact_id"
    t.index ["packet_type", "packet_id"], name: "index_mailings_on_packet_type_and_packet_id"
  end

  create_table "memberships", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "contact_id"
    t.string "gender"
    t.date "birth_on"
    t.string "street"
    t.string "housenr"
    t.string "plz"
    t.string "city"
    t.text "other_memberships"
    t.decimal "fee_amount", precision: 10, scale: 2
    t.integer "fee_period"
    t.string "fee_mode"
    t.boolean "accepts_debit"
    t.string "account_owner"
    t.string "iban"
    t.boolean "accepts_rules"
    t.boolean "wants_membership"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "recommendation"
    t.string "phone_nr"
    t.datetime "fee_check_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_memberships_on_contact_id"
  end

  create_table "messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "sender_type"
    t.bigint "sender_id"
    t.string "receiver_type"
    t.bigint "receiver_id"
    t.text "content"
    t.datetime "read_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["receiver_type", "receiver_id"], name: "index_messages_on_receiver_type_and_receiver_id"
    t.index ["sender_type", "sender_id"], name: "index_messages_on_sender_type_and_sender_id"
  end

  create_table "newsletters", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.text "content"
    t.datetime "send_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "parent_id"
    t.string "path"
    t.string "title"
    t.string "menu"
    t.integer "position", default: 1, null: false
    t.string "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parent_id"], name: "index_pages_on_parent_id"
  end

  create_table "payments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "membership_id"
    t.decimal "amount", precision: 10, scale: 2
    t.datetime "paid_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["membership_id"], name: "index_payments_on_membership_id"
  end

  create_table "proposals", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.bigint "decision_id"
    t.string "title"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_proposals_on_account_id"
    t.index ["decision_id"], name: "index_proposals_on_decision_id"
  end

  create_table "registrations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "event_id"
    t.bigint "contact_id"
    t.string "plz"
    t.string "city"
    t.string "emergency_nr"
    t.string "requests"
    t.text "participants"
    t.boolean "accepts_storage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_registrations_on_contact_id"
    t.index ["event_id"], name: "index_registrations_on_event_id"
  end

  create_table "votes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "account_id"
    t.bigint "proposal_id"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_votes_on_account_id"
    t.index ["proposal_id"], name: "index_votes_on_proposal_id"
  end

  add_foreign_key "accounts", "contacts"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "arguments", "accounts"
  add_foreign_key "arguments", "proposals"
  add_foreign_key "articles", "accounts", column: "author_id"
  add_foreign_key "comments", "accounts"
  add_foreign_key "decisions", "accounts"
  add_foreign_key "mailings", "contacts"
  add_foreign_key "memberships", "contacts"
  add_foreign_key "pages", "pages", column: "parent_id"
  add_foreign_key "payments", "memberships"
  add_foreign_key "proposals", "accounts"
  add_foreign_key "proposals", "decisions"
  add_foreign_key "registrations", "contacts"
  add_foreign_key "registrations", "events"
  add_foreign_key "votes", "accounts"
  add_foreign_key "votes", "proposals"
end
