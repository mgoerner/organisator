require 'test_helper'

class MembershipsMailerTest < ActionMailer::TestCase
  
  test "membership_accepted" do
    mail = MembershipsMailer.membership_accepted
    assert_equal "Membership accepted", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end
  
  test "payment_request" do
    mail = MembershipsMailer.payment_request
    assert_equal "Membership accepted", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end
  
end
