require "application_system_test_case"

class ContentpartsTest < ApplicationSystemTestCase
  setup do
    @contentpart = contentparts(:one)
  end

  test "visiting the index" do
    visit contentparts_url
    assert_selector "h1", text: "Contentparts"
  end

  test "creating a Contentpart" do
    visit contentparts_url
    click_on "New Contentpart"

    fill_in "Container", with: @contentpart.container_id
    fill_in "Container type", with: @contentpart.container_type
    fill_in "Content", with: @contentpart.content
    fill_in "Kind", with: @contentpart.kind
    fill_in "Position", with: @contentpart.position
    fill_in "Status", with: @contentpart.status
    fill_in "Token", with: @contentpart.token
    click_on "Create Contentpart"

    assert_text "Contentpart was successfully created"
    click_on "Back"
  end

  test "updating a Contentpart" do
    visit contentparts_url
    click_on "Edit", match: :first

    fill_in "Container", with: @contentpart.container_id
    fill_in "Container type", with: @contentpart.container_type
    fill_in "Content", with: @contentpart.content
    fill_in "Kind", with: @contentpart.kind
    fill_in "Position", with: @contentpart.position
    fill_in "Status", with: @contentpart.status
    fill_in "Token", with: @contentpart.token
    click_on "Update Contentpart"

    assert_text "Contentpart was successfully updated"
    click_on "Back"
  end

  test "destroying a Contentpart" do
    visit contentparts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Contentpart was successfully destroyed"
  end
end
