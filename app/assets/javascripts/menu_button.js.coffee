# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ menu_button.coffee'
  
  show_hide_menu = ->
    $ich = ($ this)
    sel = $ich.data 'sel'
    console.log "-- show_hide_menu - #{sel} @ menu_button.coffee"
    
    console.log "-- action: show_hide_menu - sel:#{sel}"
    ($ sel).toggleClass 'active'
    
    return false # hat_ziel
  
  
  # --- Initialisierung ------------------------
  
  $header = ($ 'header')
  unless $header.data 'init_show_hide_menu'
    console.log '-- init'
    $header.on 'click.show_hide_menu',
        '.menu_button',
        show_hide_menu
    $header.data 'init_show_hide_menu', true
  

