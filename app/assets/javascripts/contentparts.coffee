# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

($ document).on 'turbolinks:load', ->
  console.log '> script @ contentparts.coffee'
  
  show_form = ->
    $ich = ($ this)
    console.log "-- show_form - #{$ich.attr 'id'} @ contentparts.coffee"
    
    cpid = $ich.data 'cpid'
    sels = "#contentpart_#{cpid}, #edit_contentpart_#{cpid}"
    ($ sels).slideToggle 200
    
    return false # hat_ziel
  
  
  check_select_picture = ->
    $ich = ($ this)
    console.log "-- check_select_picture - #{$ich.attr 'id'} @ contentparts.coffee"
    
    option_value = $ich.val()
    console.log "-- value: #{option_value}"
    
    $file_field = $ich.closest 'form'
        .find '.field.picture'
    if option_value is 'img' or 'pdf'
      $file_field.slideDown 200
    else
      $file_field.slideUp 200
    
  
  
  # --- Initialisierung ------------------------
  
  $main = ($ 'main')
  unless $main.data 'init_contentparts'
    console.log '-- init'
    $main.on 'click.contentparts',
        '[data-editable=edit] > .contentpart',
        show_form
    $main.on 'change.contentparts',
        '.field select',
        check_select_picture
    $main.data 'init_contentparts', true
  

