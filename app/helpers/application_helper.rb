module ApplicationHelper
  
  # --- Specififcs -----------------------------
  
  def h_badge_membership_title( in_contact )
    case
      when in_contact.membership.member?
        'Mitgliedschaft'
      when in_contact.membership.accepted?
        'Antrag angenommen'
      else
        'Antrag'
    end
  end
  
  # --- TEXT FORMATTING ------------------------
  
  def h_format( in_text = '' )
    return '' if in_text.blank?
    auto_link( sanitize( in_text.strip ) ).gsub( "\n", '<br/>' ).html_safe
  end
  
  def h_transform_hs( in_text = "" )
    
  end
  
  # --- PAGE TITLE & DECRIPTION ----------------
  
  def h_page_accessible?( in_path = '' )
    page = Page.find_by( path: in_path )
    return ( page and page.published? )
  end
  
  def h_page_title_for( in_path = '' )
    page = Page.
        where( path: in_path ).
        first
    page.blank? ? nil : page.title
  end
  
  def h_page_description_for( in_path = '' )
    page = Page.
        where( path: in_path ).
        first
    page.blank? ? nil : page.description
  end
  
  # --- OpenGraph Definitionen -----------------
  
  def h_og_type
    @og_type || @og_image ? 'summary_large_image' : 'summary'
  end
  
  def h_og_title
    @og_title || @page_title || 'Organisator'
  end
  
  def h_og_description
    @og_description || 'Organisator ist eine...'
  end
  
  def h_og_image
    @og_image || image_url( 'Organisator_Logo_q320.jpg' )
  end
  
  def h_tw_url
    request.original_url
  end
  # --- setzen
  
  def h_page_title( in_title = '' )
    unless in_title.blank?
      @page_title = in_title
      
    else
      return 'Organisator' if @page_title.blank?
      return @page_title + ' @organisator'
    end
  end
  
  def h_type( in_type )
    @og_type = in_type
  end
  
  def h_title( in_title )
    @og_title = in_title
  end
  
  def h_description( in_description )
    @og_description = in_description
  end
  
  def h_image( in_image )
    @og_image = in_image
  end
  
end
