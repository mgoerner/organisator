module PagesHelper
  
  def h_page_editable?
    logger.debug "> h_page_editable? @ PagesHelper -- current_account: #{current_account.to_yaml}"
    h_is_editor?
  end
  
end
