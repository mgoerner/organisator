module ArticlesHelper
  
  def h_article_editable?
    logger.debug "> h_editable? @ ArticlesHelper -- current_account: #{current_account.to_yaml}"
    current_account and current_account == @article.author
  end
  
  def h_can_publish?
    logger.debug "> h_can_publish? @ ArticlesHelper -- current_account: #{current_account.to_yaml}"
    !@article.published? and h_is_team_member? and current_account != @article.author
  end
  
end
