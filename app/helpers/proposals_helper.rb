module ProposalsHelper
  
  def h_vote_value_class( in_vote, in_value )
    klass = ''
    klass += " a#{(in_vote / 2.0).floor + 1 }" if in_vote >= in_value
    klass += ' hilite' if in_vote == in_value
    return klass
  end
  
  def h_result_value_class( in_proposal )
    vote = in_proposal.votes_value
    klass = ''
    klass += " a#{(vote / 2.0).floor + 1 }"
    return klass
  end
  
  def h_proposal_editable?( in_proposal )
    in_proposal.decision.proposing? and in_proposal.account == current_account
  end
  
end
