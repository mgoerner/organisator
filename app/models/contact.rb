class Contact < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  has_one :account, dependent: :destroy
  has_one :membership, dependent: :destroy
  has_many :mailings, dependent: :destroy
  has_many :received_messages, as: :receiver, class_name: 'Message'
  
  # --- VALIDATIONS ----------------------------
  
  validates :token, :email,
        uniqueness: true,
        on: :create
  validates :token, :email, :first_name, :last_name,
        presence: true
  validates_acceptance_of :read_privacy
  
  # --- SCOPES ---------------------------------
  
  scope :fresh, -> { where status: 'new' }
  scope :confirmed, -> {
        where status: 'confirmed' }
  scope :wants_news, -> {
        confirmed
        .where wants_news: true }
  scope :offers_help, -> {
        where offers_help: true }
  scope :new_since, -> (date) {
        where "created_at > ?", date }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def confirmed?
    status == 'confirmed'
  end
  
  def name_text
    [ first_name, last_name ].join ' '
  end
  
  def secret
    id_hash [ first_name, last_name ].join
  end
  
  def quali_email
    '"' + name_text + '" <' + email + '>'
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.new_since_days( in_days )
    self.new_since( Time.zone.now - in_days.days ).count
  end
  
  def self.increase_since_days( in_days )
    actual_days = self.
        new_since( Time.zone.now - in_days.days ).
        count
    prev_days = self.
        new_since( Time.zone.now - ( in_days * 2).days ).
        count - actual_days
    return ( actual_days * 100.0 / prev_days ).round
  end
  
end

