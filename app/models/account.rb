class Account < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  after_destroy :ensure_an_admin_remains
  
  has_secure_password
  
  # --- RELATIONS ------------------------------
  
  belongs_to :contact
  accepts_nested_attributes_for :contact
  has_one_attached :picture
  
  has_many :articles, dependent: :nullify
  has_many :votes, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates :login, presence: true,
        uniqueness: true
  validates_presence_of :password, on: :create
  validates_confirmation_of :password, on: :create
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> { all.reject { |a| !a.published? } }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def name_text
    contact.name_text
  end
  def first_name
    contact.first_name
  end
  
  def published?
    is_team_member?
  end
  
  def accepted?
    status == 'accepted'
  end
  
  def is_admin?
    status == 'admin'
  end
  
  def is_editor?
    status == 'editor' or is_admin?
  end
  
  def is_team_member?
    status == 'team' or is_editor?
  end
  
  def is_confirmed?
    status = 'confirmed' or is_team_member?
  end
  
  def has_membership?
    contact and contact.membership
  end
  
  def membership
    contact.membership
  end
  
  def status_text
    case
      when ( status == 'confirmed' and has_membership? )
        'Mitglied'
      else
        status.capitalize
    end
  end
  
  def has_picture?
    picture.attached?
  end
  
  def picture_preview
    picture.variant( combine_options: { gravity: 'Center', resize: '800x800^', crop: '800x800+0+0', quality: '50' } )
  end
  
  # --- Voting ---------------------------------
  
  def can_vote_for?( in_proposal )
    in_proposal and in_proposal.votes.for_account( self ).all? { |v| v.status == 'new' }
  end
  
  # --- Mailings -------------------------------
  
  def send_password_reset
    if contact and contact.email.to_s.include?( '@' )
      logger.debug "> send_password_reset @ account -- id:#{id} email:#{contact.email}"
      
      self.reset_token = generate_token( 15 )
      self.reset_expires_at = Time.zone.now + 2.hours
      save!
      AccountsMailer.password_reset_mail( self ).deliver_later
    end
  end
  
  # --- Error Handling -------------------------
  
  class Error < StandardError
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.t_status_select
    STATUS_OPTIONS.collect { |o| [ self.human_attribute_name( "status.#{o}" ), o ] }
  end
  
private
  
  def ensure_an_admin_remains
    if Account.count.zero?
      raise Error.new "Can't delete last user"
    end
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS_OPTIONS = [
    'new',
    'confirmed',
    'team',
    'editor',
    'admin'
  ]
  
end
