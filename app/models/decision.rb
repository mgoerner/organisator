class Decision < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  include Publish
  after_initialize :set_publish_status
  
  belongs_to :account
  has_many :proposals, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates_presence_of :title
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where status: 'published' }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}"
  end
  
  def preparing?
    !published?
  end
  
  def proposing?
    logger.debug "-- published?: #{published?}"
    published? and Time.zone.now <= end_proposals_at
  end
  
  def voting?
    published? and ( Time.zone.now > end_proposals_at and
        Time.zone.now <= end_voting_at )
  end
  
  def votable_by?( in_account )
    voting? and not_voted_by?( in_account )
  end
  
  def active?
    published? and ( Time.zone.now <= end_voting_at )
  end
  
  def done?
    published? and ( Time.zone.now > end_voting_at )
  end
  
  def not_voted_by?( in_account )
    return true if in_account.blank?
    in_account.votes.for_decision( self ).all? { |v| v.status == 'new' }
  end
  
  def proposal_count
    proposals.published.count
  end
  
  def voters_published
    proposals.published.collect { |p|
        p.voters_published }.flatten.uniq
  end
  
  def winning_proposal
    return nil unless proposals.any? { |p| p.has_votes? }
    proposals.sort_by { |p| p.votes_value }.first
  end
  
end
