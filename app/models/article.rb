class Article < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  belongs_to :author, class_name: 'Account'
  has_many :contentparts, -> { order( position: :asc ) }, as: :container, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :title
  validates_length_of :description, maximum: 250
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> { where( status: 'published' )
        .where( 'published_at < ?', Time.zone.now ) }
  scope :new_since, -> ( d ) {
        published.
        where( 'updated_at > ?', d ).
        order( published_at: :desc ) }
  scope :for_mail_between, -> ( from, to ) {
        where( status: :published ).
        where( 'updated_at > ?', from ).
        where( 'published_at < ?', to ).
        order( published_at: :desc ) }
  scope :latest, -> { order( published_at: :desc ).
        first }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}-#{title.parameterize}"
  end
  
  def published?
    status == 'published'
  end
  
  def has_picture?
    contentparts.with_pic.count > 0
  end
  def picture
    contentparts.
        order( :position ).
        detect { |c|
            c.picture.attached? }
  end
  
end
