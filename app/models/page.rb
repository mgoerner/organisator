class Page < ApplicationRecord
  
  belongs_to :parent, class_name: 'Page', optional: true
  has_many :pages, -> { order 'position' }, foreign_key: :parent_id
  has_many :contentparts, -> { order( position: :asc ) }, as: :container, dependent: :destroy
  
  scope :published, -> {
        where status: 'published' }
  scope :root_level, -> {
        where parent_id: nil }
  
# --- INSTANCE METHODS -----------------------
  
  def iid
    "pag_#{id}"
  end
  
  def published?
    status == 'published'
  end
  
  def private?
    !published?
  end
  
  def partial_name
    path.gsub '/', '_'
  end
  
  def has_picture?
    contentparts.with_pic.count > 0
  end
  def picture
    contentparts.
        order( :position ).
        detect { |c|
            c.picture.attached? }
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS = [ 'new', 'published' ]
  
end
