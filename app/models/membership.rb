class Membership < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  # --- RELATIONS ------------------------------
  
  belongs_to :contact
  accepts_nested_attributes_for :contact
  
  has_many :payments, -> { order 'paid_at DESC' }
  
  # --- VALIDATIONS ----------------------------
  
  validates_uniqueness_of :token,
        on: :create
  validates_presence_of :token, :birth_on,
      :street, :housenr, :plz, :city,
      :other_memberships, :fee_amount,
      :fee_period, :fee_mode, :recommendation
  validates_presence_of :account_owner, :iban,
      if: Proc.new { |m| m.fee_mode == 'direct_debit' }
  validates_acceptance_of :accepts_debit,
      if: Proc.new { |m| m.fee_mode == 'direct_debit' }
  validates_acceptance_of :accepts_rules,
      :wants_membership
  
  # --- SCOPES ---------------------------------
  
  scope :unconfirmed, -> {
      references( :contact ).
      includes( :contact ).
      where( "contacts.status IN ( 'new', 'member' )" ) }
  scope :applications, -> {
      where status: 'new' }
  scope :accepted, -> {
      where status: 'accepted' }
  scope :member, -> {
      where status: 'member' }
  scope :want_newsletter, -> {
      references( :contact ).
      includes( :contact ).
      where( "contacts.wants_news = true" ) }
  scope :need_fee_check, -> {
      where( 'fee_check_at < ?', Time.zone.now ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def accepted?
    status == 'accepted'
  end
  
  def member?
    status == 'member'
  end
  
  def direct_debit?
    fee_mode == 'direct_debit'
  end
  
  def street_nr_text
    [ street, housenr ].compact.join ' '
  end
  
  def plz_city_text
    [ plz, city ].compact.join ' '
  end
  
  def secret
    id_hash [ token, contact_id.to_s, birth_on.to_s, plz ].join
  end
  
  # --- ACTIONS --------------------------------
  
  def paid_until_at
    
  end
  
  def advance_fee_check
    logger.debug "> advance_fee_check @ membership"
    last_payment = payments.first
    paid_for_months = ( last_payment.amount / ( fee_amount / fee_period ) )
    logger.debug "-- paid_for_months: #{paid_for_months}"
    first_of_month_at = Time.zone.now.at_beginning_of_month
    self.update_attribute :fee_check_at, first_of_month_at + ( paid_for_months * 30 ).days + 5.days + rand( 300 ).minutes
  end
  
  def send_fee_request
    logger.debug "> send_fee_request @ membership"
    self.update_attribute :fee_check_at, Time.zone.now + 5.days + rand( 300 ).minutes
    MembershipsMailer.
        with( membership: self ).
        payment_request.
        deliver_later
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.t_period_select
    PERIOD_OPTIONS.collect { |o| [ self.human_attribute_name( "fee_period.#{o}" ), o ] }
  end
  
  def self.t_gender_select
    GENDER_OPTIONS.collect { |o| [ self.human_attribute_name( "gender.#{o}" ), o ] }
  end
  
  def self.t_mode_select
    MODE_OPTIONS.collect { |o| [ self.human_attribute_name( "fee_mode.#{o}" ), o ] }
  end
  
  def self.generate_funding
    self.where( status: [ :member, :published ] ).inject( 0 ) { |sum, m|
        sum + m.fee_amount.to_f / m.fee_period.to_f }
  end
  
  def self.potential_funding
    self.all.inject( 0 ) { |sum, m|
        sum + m.fee_amount.to_f / m.fee_period.to_f }
  end
  
  # --- CONSTANTS ------------------------------
  
  GENDER_OPTIONS = [
    'female',
    'male',
    'diverse'
  ]
  
  PERIOD_OPTIONS = [
    1,
    3,
    6,
    12
  ]
  
  MODE_OPTIONS = [
    'transferral',
    'paypal',
    'direct_debit'
  ]
  
end

