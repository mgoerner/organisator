class Event < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  include Publish
  after_initialize :set_publish_status
  
  attr_accessor :length
  
  before_save :calc_ends_at
  
  has_many :registrations
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :title
  validates_acceptance_of :read_privacy
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where status: 'published' }
  scope :upcoming, -> {
        where( 'starts_at > ?', Time.zone.now ).
        order 'starts_at ASC' }
  scope :for_mail_from, -> ( from ) {
        where( 'starts_at > ?', from ).
        where( 'starts_at < ?', from + OUTLOOK_DAYS ).
        order 'starts_at ASC' }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}-#{title.parameterize}"
  end
  
  def longer_than_normal
    starts_at.at_beginning_of_day != ends_at.at_beginning_of_day
  end
  
  def calc_ends_at
    self.ends_at = starts_at + length.to_i.hours
  end
  
  def starts_at_text
    text = I18n.l( starts_at, format: :html_break_long )
    text += '<br/>&mdash; ' + I18n.l( ends_at, format: :html_break_long ) if longer_than_normal
    return text.html_safe
  end
  
  def registration_enabled?
    max_registrations.to_i > 0
  end
  
  def remaining_registrations
    max_registrations.to_i - registrations.count
  end
  
  def sold_out?
    registrations.count >= max_registrations.to_i
  end
  
  def registration_not_started?
    !registration_starts_at.blank? and
    registration_starts_at > Time.zone.now
  end
  
  def registration_passed?
    !registration_ends_at.blank? and
    registration_ends_at < Time.zone.now
  end
  
  def registration_possible?
    # return false if not enabled
    return false unless max_registrations
    
    # return whether reg still possible
    registration_enabled? and
    remaining_registrations > 0 and
    !registration_not_started? and
    !registration_passed?
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.select_length
    LENGTH_VALUES
  end
  
  # --- CONSTANTS ------------------------------
  
  OUTLOOK_DAYS = 28.days
  
  LENGTH_VALUES = [
    [ "1 Stunde", 1 ],
    [ "2 Stunden", 2 ],
    [ "3 Stunden", 3 ],
    [ "4 Stunden", 4 ],
    [ "8 Stunden", 8 ],
    [ "1 Tag", 24 ],
    [ "2 Tage", 48 ],
    [ "3 Tage", 72 ],
    [ "4 Tage", 96 ],
    [ "5 Tage", 120 ],
    [ "1 Woche", 168 ] ]
  
end
