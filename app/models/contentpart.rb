class Contentpart < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  belongs_to :container, polymorphic: true
  has_one_attached :picture
  
  acts_as_list scope: :container
  
  # --- SCOPES ---------------------------------
  
  scope :with_pic, -> { where kind: 'img' }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "cp_#{token}"
  end
  
  def to_param
    "#{token}"
  end
  
  def tag_type
    case kind
      when 'p' then 'p'
      else kind
    end
  end
  
  def klass
    klass = case kind
      when 'ol' then 'ol'
      else nil
    end
    [ 'contentpart', klass ].join ' '
  end
  
  # --- Picture Variants -----------------------
  
  def picture_article
    picture.variant( combine_options: { resize: '1200>', quality: '50' } )
  end
  
  def picture_preview
    if kind == 'img'
      picture.variant( combine_options: { gravity: 'Center', resize: '800x600^', crop: '800x600+0+0', quality: '50' } )
    elsif kind == 'pdf'
      picture.preview( resize: '200x200>' )
    end
  end
  
  def picture_twitter
    picture.variant( combine_options: { gravity: 'Center', resize: '1200x628^', crop: '1200x628+0+0', quality: '50' } )
  end
  
  def file_name
    picture.filename
  end
  
  def file_size
    picture.byte_size
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.select_kind
    KINDS
  end
  
  # --- CONSTANTS ------------------------------
  
  KINDS = [
      [ 'Paragraph', 'p' ],
      [ 'Bild mit Grösse', 'sized_img' ],
      [ 'Bild', 'img' ],
      [ 'Auflistung ul', 'ul' ],
      [ 'Überschrift h2', 'h2' ],
      [ 'Überschrift h3', 'h3' ],
      [ 'Überschrift h4', 'h4' ],
      [ 'PDF Dokument', 'pdf' ]
  ]
  
end
