class Proposal < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  include Publish
  after_initialize :set_publish_status
  
  belongs_to :account
  belongs_to :decision
  has_many :votes, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates_presence_of :title
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where status: 'published' }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "pr_#{token}"
  end
  
  def to_param
    "#{token}"
  end
  
  def position
    index = decision.proposals.find_index { |p| p == self }
    return index + 1
  end
  
  def has_votes?
    votes.published.count > 0
  end
  
  def voters_published
    unless defined? @voters_published
      @voters_published = votes.published.includes( :account).collect { |v|
          v.account }
    end
    return @voters_published
  end
  
  def vote_for( in_account )
    votes.for_account( in_account ).first
  end
  
  def vote_value_for( in_account )
    vote = vote_for( in_account )
    return 0 if vote.blank?
    return vote.value
  end
  
  def votes_value
    unless defined? @votes_value
      unless has_votes? or decision.voters_published.count == 0
        @votes_value = 0
        
      else
        value_sum = votes.published.inject(0) { |sum, v|
          sum + v.value }
        @votes_value = ( value_sum * 100.0 / decision.voters_published.count ).round * 1.0 / 100.0
      end
    end
    return @votes_value
  end
  
end
