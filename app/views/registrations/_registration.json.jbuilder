json.extract! registration, :id, :token, :status, :event_id, :contact_id, :plz, :city, :emergency_nr, :participants, :accepts_storage, :created_at, :updated_at
json.url registration_url(registration, format: :json)
