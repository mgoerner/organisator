json.extract! membership, :id, :token, :status, :contact_id, :gender, :birth_on, :street, :housenr, :plz, :city, :other_memberships, :fee_amount, :fee_period, :fee_mode, :wants_membership, :starts_at, :ends_at, :fee_check_at, :created_at, :updated_at
json.url membership_url(membership, format: :json)
