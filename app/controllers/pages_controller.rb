class PagesController < ApplicationController
  before_action :set_page, only: [ :show, :edit, :update, :destroy ]
  
  skip_before_action :require_team
  
  # --- CUSTOM METHODS -------------------------
  
  def root
    @page = Page.published.find_by path: ""
    
    if @page.blank?
      render :root
    else
      render :show
    end
  end
  
  # --- CRUD METHODS ---------------------------
  
  # GET /pages
  # GET /pages.json
  def index
    redirect_to root_path, notice: 'Kein Zugriff, sorry!' and return unless h_is_editor?
    
    @pages = Page.root_level.order position: :asc
  end
  
  # GET /pages/1
  # GET /pages/1.json
  def show
    render_404 and return if not_accessible?
  end
  
  # GET /pages/new
  def new
    @page = Page.new
  end
  
  # GET /pages/1/edit
  def edit
  end
  
  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new( page_params )
    
    respond_to do |format|
      if @page.save
        format.html { redirect_to pages_path, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update( page_params )
        format.html { redirect_to pages_path, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  def not_accessible?
    @page.blank? or
        ( @page.private? and
            !( current_account and
               current_account.is_editor? ) )
  end
  
  # Use callbacks to share common setup or constraints between actions.
  def set_page
    if !params[ :path ].blank?
      @page = Page.find_by path: params[ :path ]
    elsif params[ :id ].to_i > 0
      @page = Page.find params[ :id ]
    else
      @page = Page.find_by path: params[ :id ]
    end
    render_404 and return if @page.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def page_params
    params.require( :page ).permit( :status, :parent_id, :path, :title, :menu, :position, :description )
  end
  
end
