class DecisionsController < ApplicationController
  before_action :set_decision, only: [ :publish, :vote, :show, :edit, :update, :destroy ]
  skip_before_action :require_team
  before_action :require_member
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /decisions/1/publish
  def publish
    redirect_to login_path, alert: 'Sie müssen Mitglied sein für diese Aktion!' and return unless h_is_member?
    
    @decision.status = 'published'
    
    respond_to do |format|
      if @decision.save
        format.html { redirect_to @decision, notice: 'Entscheidung wurde bereit gemacht.' }
        format.json { render :show, status: :published, location: @decision }
      else
        format.html { redirect_to @decision, alert: 'Entscheidung konnte bereit gemacht werden.' }
        format.json { render json: @decision.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PUT /decisions/1/vote
  def vote
    redirect_to login_path, alert: 'Sie müssen Mitglied sein für diese Aktion!' and return unless h_is_member?
    
    current_account.votes.for_decision( @decision ).each do |vote|
      vote.status = 'published'
      vote.save!
    end
    
    redirect_to @decision, notice: 'Deine Stimmen wurden angenommen.'
  end
  
  # --- INSTANCE METHODS -----------------------
  
  # GET /decisions
  # GET /decisions.json
  def index
    @decisions = Decision.published.order updated_at: :desc
  end
  
  # GET /decisions/1
  # GET /decisions/1.json
  def show
    if @decision.done?
      @decision = Decision.
          where( token: params[ :id ] ).
          references( proposals: [ votes: [ :account ] ] ).
          includes( proposals: [ votes: [ :account ] ] ).first
    end
    
    
  end
  
  # GET /decisions/new
  def new
    @decision = Decision.new
    limit = ( Time.zone.now + 1.hour ).beginning_of_hour
    @decision.end_proposals_at = limit + 1.day
    @decision.end_voting_at = limit + 2.days
  end
  
  # GET /decisions/1/edit
  def edit
  end
  
  # POST /decisions
  # POST /decisions.json
  def create
    @decision = Decision.new( decision_params )
    @decision.account = current_account
    
    respond_to do |format|
      if @decision.save
        format.html { redirect_to @decision, notice: 'Decision was successfully created.' }
        format.json { render :show, status: :created, location: @decision }
      else
        format.html { render :new }
        format.json { render json: @decision.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /decisions/1
  # PATCH/PUT /decisions/1.json
  def update
    respond_to do |format|
      if @decision.update(decision_params)
        format.html { redirect_to @decision, notice: 'Decision was successfully updated.' }
        format.json { render :show, status: :ok, location: @decision }
      else
        format.html { render :edit }
        format.json { render json: @decision.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /decisions/1
  # DELETE /decisions/1.json
  def destroy
    @decision.destroy
    respond_to do |format|
      format.html { redirect_to decisions_url, notice: 'Decision was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_decision
    @decision = Decision.find_by token: params[ :id ]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def decision_params
    params.require( :decision ).permit( :title, :description, :end_proposals_at, :end_voting_at )
  end
  
end
