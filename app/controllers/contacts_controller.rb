class ContactsController < ApplicationController
  include ContactHelper
  
  skip_before_action :require_team, only: [ :activate, :unsubscribe, :show, :new, :edit, :create, :update ]
  before_action :set_contact, only: [ :activate, :unsubscribe, :show, :edit, :update, :destroy ]
  
  # GET /contacts/1/activate
  def activate
    @successful = false
    
    if @contact.secret == params[ :ring ]
      unless @contact.account
        @contact.status = 'confirmed'
        @successful = @contact.save
        
      else
        @contact.status = 'confirmed'
        @contact.account.status = 'confirmed'
        @successful = @contact.account.save and @contact.save
      end
    end
  end
  
  # GET /contacts/1/unsubscribe
  def unsubscribe
    redirect_to( root_path, alert: 'Ungültiger Link!' ) and return if @contact.id_hash != params[ :ato ]
    
    session[ :contact_destroy_id ] = @contact.id
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /contacts
  # GET /contacts.json
  def index
    letter_selected = params[ :letter ]
    
    @letters = Contact.find_by_sql "
        select left( last_name, 1 ) as letter
          from contacts
          group by letter
          order by letter asc"
    if letter_selected == '*'
      @contacts = Contact.
          where( "length( last_name ) = 0", letter_selected ).
          order :last_name
    else
      @contacts = Contact.
          where( "left( last_name, 1 ) = ?", letter_selected ).
          order :last_name
    end
  end
  
  # GET /contacts/1
  # GET /contacts/1.json
  def show
    unless h_is_admin?
      redirect_to @contact.account and return unless @contact.account.blank?
      redirect_to root_path, alert: 'Kein Zugriff, sorry!' and return
    end
  end
  
  # GET /contacts/new
  def new
    @contact = Contact.new( { wants_news: true } )
  end
  
  # GET /contacts/1/edit
  def edit
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_contact_editable?( @contact )
  end
  
  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new( contact_params )
    @contact.status = 'new'
    
    respond_to do |format|
      if @contact.save
        ContactsMailer.with( contact: @contact ).
            confirmation_mail.
            deliver_later
        
        format.html { render :registered }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    letter = @contact.email[ 0 ]
    @contact.destroy
    
    respond_to do |format|
      format.html { redirect_to contacts_path( letter: letter ), notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find_by token: params[ :id ]
    render_404 and return if @contact.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require( :contact ).permit( :email, :first_name, :last_name, :plz, :read_privacy, :wants_news, :offers_help, :wants_form, :street, :housenr, :city )
  end
  
end
