# ----------------------------------------------
class SessionsController < ApplicationController
  skip_before_action :require_team,
        only: [ :new, :create, :destroy ]
  
  def new
    write_log 'session new'
  end
  
  def create
    account = Account.find_by login: params[ :login ]
    if account.try :authenticate, params[ :password ]
      session[ :account_id ] = account.id
      write_log 'session create'
      redirect_to account
      
    else
      redirect_to login_url,
            alert: "Unbekannte E-Mail/Password Kombination"
    end
  end
  
  def destroy
    session[ :account_id ] = nil
    write_log 'session destroy'
    redirect_to root_url, notice: "Du hast Dich abgemeldet"
  end
  
end
