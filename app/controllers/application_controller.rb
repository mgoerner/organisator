class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  before_action :extract_token
  before_action :require_team, except: [ :search ]
  before_action :load_pages
  
  after_action :check_mailings
  
  # --- SITE WIDE METHODS ----------------------
  
  def search
    if params[ :q ]
      q = "%#{params[ :q ]}%"
      @results = []
      @results |= Page.where( "title LIKE :q OR menu LIKE :q OR description LIKE :q", { q: q } ).published
      @results |= Account.where( "displayname LIKE ?", q ).published
      @results |= Article.where( [ "title LIKE :q OR description LIKE :q", { q: q } ] ).published
      @results |= Event.where( [ "title LIKE :q OR description LIKE :q OR location LIKE :q", { q: q } ] ).published
      if h_is_admin?
        @results |= Contact.where( [ "first_name LIKE :q OR last_name LIKE :q OR email LIKE :q", { q: q } ] )
        @results |= Membership.where( [ "token LIKE :q OR city LIKE :q", { q: q } ] )
      end
      
      @results.sort! { |a,b| b.updated_at <=> a.updated_at }
    end
    
    render 'general/search'
  end
  
protected
  
  def extract_token
    if params[ :t ]
      logger.debug "> extract_token @ app con -- token:#{params[ :t ]}"
      token = params[ :t ]
      @mailing = Mailing.sent.last_month.detect { |v|
            v.token == params[ :t ] }
      if @mailing
        logger.debug "-- @mailing:#@mailing.to_yaml}"
        @mailing.update_attribute :clicks, @mailing.clicks.to_i + 1
      end
    end
  end
  
  def h_orga_name
    'Organisation'
  end
  helper_method :h_orga_name
  
  # --- ACCOUNT DETECTION ----------------------
  
  def set_current_account( in_account )
    delete session[ :account_id ] and return unless in_account
    session[ :account_id ] = in_account.id
  end
  
  def current_account
    unless defined? @_current_account
      @_current_account = Account.find_by id: session[ :account_id ]
    end
    return @_current_account
  end
  helper_method :current_account
  
  def h_is_admin?
    current_account and current_account.is_admin?
  end
  helper_method :h_is_admin?
  
  def h_is_editor?
    current_account and current_account.is_editor?
  end
  helper_method :h_is_editor?
  
  def h_is_team_member?
    current_account and current_account.is_team_member?
  end
  helper_method :h_is_team_member?
  
  def h_is_member?
    current_account and
        current_account.has_membership? and
        ( current_account.membership.member? or
            current_account.membership.accepted? )
  end
  helper_method :h_is_member?
  
  def h_is_team_or_member?
    ( current_account and current_account.is_team_member? ) or h_is_member?
  end
  helper_method :h_is_team_or_member?
  
  def h_is_user?
    current_account and current_account.is_confirmed?
  end
  helper_method :h_is_user?
  
  # --- AUTHORIZATION --------------------------
  
  def require_member
    redirect_to login_path,
            notice: 'Bitte einloggen.' and return unless current_account
    redirect_to login_path,
            alert: 'Für diese Aktion musst du Mitglied sein.' and return unless h_is_member?
  end
  
  def require_team
    redirect_to login_path,
            notice: 'Bitte einloggen.' and return unless current_account
    
    redirect_to login_path,
            alert: 'Für diese Aktion musst du Team-Mitglied sein.' and return unless h_is_team_member? # halts request cycle
  end
    
  def require_admin
    unless current_account.is_admin?
      redirect_to login_path,
            alert: "You must be appointed admin to access this section" # halts request cycle
    end
  end
  
  # --- PAGES ----------------------------------
  
  def load_pages
    @pages = Page.published.root_level.order position: :asc
  end
  
  def render_404
    render(
        file: "#{Rails.root}/public/404.html" ,
        status: :not_found,
        layout: false )
  end
  
  # --- MAILINGS -------------------------------
  
  def check_mailings
    logger.debug "\n> check_mailings @ app controller"
    
    # steht ein Newsletter zum Versand an?
    @newsletters = Newsletter.for_mailing
    logger.debug "-- @newsletters:#{@newsletters.to_yaml}"
    unless @newsletters.blank?
      @newsletters.each { |n| n.start_mailing }
    end
    
    # stehen Versände aus?
    @mailings = Mailing.to_send
    logger.debug "-- @mailings:#{@mailings.to_yaml}"
    unless @mailings.blank?
      logger.debug "-- @mailings.size:#{@mailings.size}"
      @mailings.each { |m| m.send_out }
    end
    
    # are there membership fees to be reminded?
    @members_overdue = Membership.need_fee_check
    logger.debug "-- @members_overdue:#{@members_overdue.to_yaml}"
    unless @members_overdue.blank?
      @members_overdue.each { |m| m.send_fee_request }
    end
    
  end
  
  # --- LOGS -----------------------------------
  
  def write_log( in_action,
        in_comment = nil,
        in_entity = nil )
    logger.debug "> write_log @ app controller"
    
    user_agent = request.env[ 'HTTP_USER_AGENT' ]
    unless user_agent =~ /Mediatoolkitbot|check_http|curl/
#       Log.create!( { actor: current_account,
#             action: in_action,
#             comment: in_comment,
#             entity: in_entity,
#             user_agent: ( request.blank? ? nil : user_agent ),
#             ip: ( request.blank? ? nil : request.headers[ 'X-Real-IP' ] ) } )
    end
  end
  

end
