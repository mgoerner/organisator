class ArticlesController < ApplicationController
  before_action :set_article, only: [ :publish, :show, :edit, :update, :destroy ]
  
  skip_before_action :require_team
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /articles/1/publish
  def publish
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless h_is_team_member? and current_account != @article.author
    
    @article.status = 'published'
    
    respond_to do |format|
      if @article.save
        format.html { redirect_to articles_path, notice: 'Article was successfully published.' }
        format.json { render :show, status: :ok, location: @article }
      else
        flash.alert = 'Did not work!'
        format.html { render :show }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # --- CRUD METHODS ---------------------------
  
  # GET /articles
  # GET /articles.json
  def index
    if h_is_team_member?
      @articles = Article.all
      
    else
      # 404 if the page is not accessible from
      # the outside
      render_404 and return if not_accessible?
      
      @articles = Article.none
      @articles = @articles.where( author: current_account ) if current_account
      @articles = @articles.or( Article.published )
    end
    @articles = @articles.order published_at: :desc
  end
  
  # GET /articles/1
  # GET /articles/1.json
  def show
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless @article.published? or current_account == @article.author or h_is_team_member?
  end
  
  # GET /articles/new
  def new
    @article = Article.new
  end
  
  # GET /articles/1/edit
  def edit
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless current_account == @article.author or h_is_editor?
  end
  
  # POST /articles
  # POST /articles.json
  def create
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless h_is_user?
    
    @article = Article.new( article_params )
    @article.status = 'new'
    @article.author = current_account
    
    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless current_account == @article.author or h_is_editor?
    
    respond_to do |format|
      if @article.update( article_params )
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    redirect_to articles_path, notice: 'Kein Zugriff, sorry.' and return unless current_account == @article.author or h_is_editor?
    
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  def not_accessible?
    articles_page = Page.find_by path: 'articles'
    return ( articles_page.blank? or articles_page.private? )
  end
  
  # Use callbacks to share common setup or constraints between actions.
  def set_article
    @article = Article.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @article.blank?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def article_params
    params.require( :article ).permit( :title, :published_at, :description )
  end
  
end
