class NewslettersController < ApplicationController
  before_action :set_newsletter, only: [ :ready, :test, :show, :edit, :update, :destroy ]
  
  skip_before_action :require_team, only: [ :index, :show ]
  
  # --- CUSTOM METHODS -------------------------
  
  # PUT /newsletters/1/ready
  def ready
    @newsletter.update_attribute :status, 'ready'
    
    redirect_to @newsletter
  end
  
  def test
    test_addresses = params[ :addresses ].split( ' ' )
    
    contacts = Contact.where email: test_addresses
    mailings = []
    
    contacts.each do |contact|
      mailings << Mailing.create!( {
        status: 'ready',
        contact: contact,
        packet: @newsletter,
        send_at: Time.zone.now } )
    end
    
    mailings.each { |m| m.send_out }
    
    respond_to do |format|
      if @newsletter.save
        format.html { redirect_to @newsletter, notice: "Test-Newsletter an #{contacts.size} Empfänger gesendet." }
        format.json { render :show, status: :created, location: @newsletter }
      else
        format.html { render :new }
        format.json { render json: @newsletter.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /newsletters
  # GET /newsletters.json
  def index
    if h_is_team_member?
      @newsletters = Newsletter.all
      
    else
      @newsletters = Newsletter
            .published
            .order( send_at: :desc )
    end
    @newsletters = @newsletters.order send_at: :desc
  end
  
  # GET /newsletters/1
  # GET /newsletters/1.json
  def show
    redirect_to newsletters_path, notice: 'Kein Zugriff, sorry.' and return unless @newsletter.public? or h_is_team_member?
  end
  
  # GET /newsletters/new
  def new
    @newsletter = Newsletter.new
  end
  
  # GET /newsletters/1/edit
  def edit
    redirect_to newsletters_path, notice: 'Kein Zugriff, sorry.' and return unless h_is_editor?
  end
  
  # POST /newsletters
  # POST /newsletters.json
  def create
    redirect_to newsletters_path, notice: 'Kein Zugriff, sorry.' and return unless h_is_editor?
    
    @newsletter = Newsletter.new( newsletter_params )
    @newsletter.status = 'new'
    
    respond_to do |format|
      if @newsletter.save
        format.html { redirect_to @newsletter, notice: 'Newsletter was successfully created.' }
        format.json { render :show, status: :created, location: @newsletter }
      else
        format.html { render :new }
        format.json { render json: @newsletter.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /newsletters/1
  # PATCH/PUT /newsletters/1.json
  def update
    redirect_to newsletters_path, notice: 'Kein Zugriff, sorry.' and return unless h_is_editor?
    
    respond_to do |format|
      if @newsletter.update( newsletter_params )
        format.html { redirect_to @newsletter, notice: 'Newsletter was successfully updated.' }
        format.json { render :show, status: :ok, location: @newsletter }
      else
        format.html { render :edit }
        format.json { render json: @newsletter.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /newsletters/1
  # DELETE /newsletters/1.json
  def destroy
    redirect_to newsletters_path, notice: 'Kein Zugriff, sorry.' and return unless !@newsletter.is_public? and h_is_editor?
    
    @newsletter.destroy
    respond_to do |format|
      format.html { redirect_to newsletters_url, notice: 'Newsletter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_newsletter
    @newsletter = Newsletter.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @newsletter.blank?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def newsletter_params
    params.require( :newsletter ).permit( :content, :send_at )
  end
  
end
