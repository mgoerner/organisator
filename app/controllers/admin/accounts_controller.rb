class Admin::AccountsController < ApplicationController
  include AccountHelper
  
  before_action :require_admin
  before_action :set_account, only: [ :show, :edit, :update, :destroy ]
  
  # --- CUSTOM METHODS -------------------------
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /accounts
  # GET /accounts.json
  def index
    @status_selected = params[ :status ]
    if @status_selected.blank?
      @accounts = Account.order 'status, updated_at DESC'
    else
      @accounts = Account.where status: @status_selected
    end
  end
  
  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end
  
  # GET /accounts/new
  def new
    @account = Account.new( contact: Contact.new )
    unless params[ :email ].blank?
      @account.contact.email = params[ :email ]
      @account.login = params[ :email ]
    end
  end
  
  # GET /accounts/1/edit
  def edit
  end
  
  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new( account_params )
    @account.status = 'new'
    @account.contact.status = 'new' if @account.contact
    
    respond_to do |format|
      if @account.save
        ContactsMailer.with( contact: @account.contact ).
            confirmation_mail.
            deliver_later
        
        set_current_account @account
        
        format.html { redirect_to @account, notice: 'Account was successfully created.' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update( account_params )
        format.html { redirect_to admin_account_path( @account ), notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find_by token: params[ :id ]
    render_404 and return if @account.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require( :account ).permit( :status, :login, :password, :password_confirmation, :displayname, :label, :description, :picture, :contact_attributes => [ :email, :first_name, :last_name, :is_member, :read_basics, :read_privacy, :wants_news ] )
  end
  
end
