class ApplicationMailer < ActionMailer::Base
  default from: '"Lisa Muster" <lisa@organisator.org>'
  layout 'mailer'
end
